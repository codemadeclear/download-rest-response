# How to download a file from a REST endpoint in Spring Boot

Demo project to show how to create a REST endpoint in Spring Boot that allows to download the response data as a file.

For more details check out the following blog post:

- downloading a csv file from a REST endpoint: https://codemadeclear.com/index.php/2021/02/22/file-download-from-a-rest-api-with-spring-boot/
