package com.codemadeclear.downloadrestresponse;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DownloadRestResponseApplication {

	public static void main(String[] args) {
		SpringApplication.run(DownloadRestResponseApplication.class, args);
	}

}
