package com.codemadeclear.downloadrestresponse.controllers;

import com.codemadeclear.downloadrestresponse.model.Employee;
import com.codemadeclear.downloadrestresponse.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

@RestController
@RequestMapping("/employees")
public class EmployeeController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping
    public ResponseEntity<?> findAll(@RequestParam(required = false) DownloadFormat download) {
        List<Employee> employees = employeeService.findAll();
        return getResponse(employees, download);
    }

    private ResponseEntity<?> getResponse(List<Employee> employees,
                                          DownloadFormat downloadFormat) {
        if (downloadFormat == null) {
            return ResponseEntity.ok(employees);
        }
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_TYPE, downloadFormat.contentType)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=employees.".concat(downloadFormat.toString()))
                .body(new ByteArrayResource(downloadFormat.employeesToByteArray.apply(employees)));
    }

    @SuppressWarnings("unused")
    enum DownloadFormat {
        csv(MediaType.TEXT_PLAIN_VALUE,
                employees ->
                        employees.stream()
                                .map(employee -> String.join(";",
                                        employee.getId().toString(),
                                        employee.getFirstName(),
                                        employee.getLastName(),
                                        employee.getDateOfBirth().toString()))
                                .collect(Collectors.joining("\n"))
                                .getBytes());

        private final String contentType;
        private final Function<List<Employee>, byte[]> employeesToByteArray;

        DownloadFormat(String contentType,
                       Function<List<Employee>, byte[]> employeesToByteArray) {
            this.employeesToByteArray = employeesToByteArray;
            this.contentType = contentType;
        }
    }

}
