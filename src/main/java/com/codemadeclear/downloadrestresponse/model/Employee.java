package com.codemadeclear.downloadrestresponse.model;

import lombok.Data;
import lombok.experimental.Accessors;

import java.time.LocalDate;

@Data
@Accessors(chain = true)
public class Employee {

    private Long id;
    private String firstName;
    private String lastName;
    private LocalDate dateOfBirth;

}
