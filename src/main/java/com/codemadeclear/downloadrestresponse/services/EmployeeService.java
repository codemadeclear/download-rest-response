package com.codemadeclear.downloadrestresponse.services;

import com.codemadeclear.downloadrestresponse.model.Employee;

import java.util.List;

public interface EmployeeService {

    List<Employee> findAll();

}
