package com.codemadeclear.downloadrestresponse.services.impl;

import com.codemadeclear.downloadrestresponse.model.Employee;
import com.codemadeclear.downloadrestresponse.services.EmployeeService;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.Month;
import java.util.ArrayList;
import java.util.List;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final List<Employee> EMPLOYEES = new ArrayList<>();

    static {
        long i = 0;
        EMPLOYEES.add(new Employee()
                .setId(++i)
                .setFirstName("John")
                .setLastName("Doe")
                .setDateOfBirth(LocalDate.of(1980, Month.JULY, 14)));
        EMPLOYEES.add(new Employee()
                .setId(++i)
                .setFirstName("Mary")
                .setLastName("Williams")
                .setDateOfBirth(LocalDate.of(1988, Month.FEBRUARY, 22)));
        EMPLOYEES.add(new Employee()
                .setId(++i)
                .setFirstName("Edward")
                .setLastName("Roberts")
                .setDateOfBirth(LocalDate.of(1990, Month.DECEMBER, 19)));
        EMPLOYEES.add(new Employee()
                .setId(++i)
                .setFirstName("Elizabeth")
                .setLastName("McGee")
                .setDateOfBirth(LocalDate.of(1982, Month.OCTOBER, 3)));
    }

    @Override
    public List<Employee> findAll() {
        return EMPLOYEES;
    }

}
